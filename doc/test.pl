#!/usr/bin/perl

use strict;
use warnings;
open FILE, "<1.txt";

#undef $/;

#$/ = '';            # paragraph read mode for readline access


my %data = ();


while(<FILE>)
{
#1.189163500000000,Async Serial,0xEA
#1.189167500000000,Async Serial,0x00
#1.189171500000000,Async Serial,0xEA
#1.189178562500000,Async Serial,0xEA
#1.189182562500000,Async Serial,0x00
#1.189186562500000,Async Serial,0x00
#1.189190562500000,Async Serial,0xEA



#	while (m/^\S+\,Async Serial\,(0xEA).+\S+\,Async Serial\,(0x01).+(?:\S+\,Async Serial\,(0x.+)){2}$/sm)	{
  if (m/\S+\,Async Serial\,(0xEA)/ms) {
  my $index;
  my $value;
		print "FOUND:$1\n";
		for(my $i=0;$i<6;$i++) {
			my $p = <FILE>;

		  if ($i == 3) {
				if( $p =~ m/\S+\,Async Serial\,(0x.+)/ ) {
					$index = $1;
				}
				print "INDEX: $index\n";
			}
      if ($i == 4) {
        if( $p =~ m/\S+\,Async Serial\,(0x.+)/ ) {
          $value = $1;
        }
        print "VALUE: $value\n";
      $data{$index} = $value;
      }
		}
	}
}

close FILE;

foreach my $key (sort(keys %data))
{
  # do whatever you want with $key and $value here ...
  my $value = $data{$key};
  print "eeprom[$key] = $value;\n";
}