target extended-remote tcp:localhost:2000
set target-async on
set mem inaccessible-by-default off
set remote hardware-breakpoint-limit 6
set remote hardware-watchpoint-limit 4
monitor swdp_scan
monitor traceswo
attach 1
stop
load
monitor reset halt



