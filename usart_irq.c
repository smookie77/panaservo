/*
 * This file is part of the libopencm3 project.
 *
 * Copyright (C) 2009 Uwe Hermann <uwe@hermann-uwe.de>
 * Copyright (C) 2011 Stephen Caudle <scaudle@doceme.com>
 * Copyright (C) 2013 Piotr Esden-Tempski <piotr@esden.net>
 *
 * This library is free software: you can redistribute it and/or modify
 * it under the terms of the GNU Lesser General Public License as published by
 * the Free Software Foundation, either version 3 of the License, or
 * (at your option) any later version.
 *
 * This library is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU Lesser General Public License for more details.
 *
 * You should have received a copy of the GNU Lesser General Public License
 * along with this library.  If not, see <http://www.gnu.org/licenses/>.
 *
 * STM32F405/415, STM32F407/417, STM32F427/437 and STM32F429/439 reference manual (RM0090)
 */

#include <libopencm3/stm32/rcc.h>
#include <libopencm3/stm32/gpio.h>
#include <libopencm3/stm32/usart.h>
#include <libopencm3/stm32/dma.h>
#include <libopencm3/stm32/timer.h>
#include <libopencm3/stm32/lptimer.h>

//#include <libopencm3/cm3/itm.h>
#include <libopencm3/cm3/nvic.h>

#define BKPT __asm("bkpt 255")

#define SWO_FREQ 2250000
#define HCLK_FREQ 96000000

//https://github.com/nickd4/bmp_traceswo

/* Pelican TPIU
 * cut down TPIU implemented in STM32F7 series
 * see en.DM00224583.pdf page 1882
 */
#define TPIU_CURRENT_PORT_SIZE *((volatile unsigned *)(0xE0040004))
#define TPIU_ASYNC_CLOCK_PRESCALER *((volatile unsigned *)(0xE0040010))
#define TPIU_SELECTED_PIN_PROTOCOL *((volatile unsigned *)(0xE00400F0))
#define TPIU_FORMATTER_AND_FLUSH_CONTROL *((volatile unsigned *)(0xE0040304))


#define ARRAY_LEN(x)  (sizeof(x) / sizeof((x)[0]))

//2.5MBps 8N1
//basic frame - i though that it is tamagawa... have to rename
struct tmgw_struc {
   uint8_t cf;
   uint8_t sf;
   uint8_t d0;
   uint8_t d1;
   uint8_t d2;
   uint8_t d3;
   uint8_t d4;
   uint8_t d5;
   uint8_t d6;
   uint8_t d7;
   uint8_t crc;
   uint8_t len;
};

//struct of structures :)
struct tmgw_struc2 {
    struct tmgw_struc rx;
    struct tmgw_struc tx;
    struct tmgw_struc tx2;
};

//last one..
struct tmgw_struc2 tmgw;

static uint8_t crcCalc(uint8_t* Data, uint8_t Size);

//DMA related
void dma_receive(void* buffer, const int datasize);
void dma_receive_setup(void);
int dma_done(void);

//USART related
void usart_rx_check(void);
void usart_process_data(const uint8_t* data, uint8_t len);

uint8_t usart_rx_dma_buffer[64];

// PANASONIC FRAME BUFFER
uint8_t frame_buffer[128];
static uint8_t frame_buffer_pos = 0;
static uint8_t frame_buffer_crc = 0;

//from where process_data is called. usart1_idle, dma_ht, dma_tc 1, 2, 3 check order
uint8_t from = 0;

// EEPROM ?
uint8_t eeprom[0x7f];
void initialize_eeprom();

//junk yard
uint8_t tim2_1 = 0x02;
uint8_t tim2_2 = 0x00;
uint8_t tim2_3 = 0x00;


//encoder related
uint32_t motor_pos = 0;
uint32_t motor_pos_old = 0;

//
static void clock_setup(void)
{
    rcc_clock_setup_pll(&rcc_hse_8mhz_3v3[RCC_CLOCK_3V3_168MHZ]);

    /* Enable GPIOB clock for LED & USARTs. */
    rcc_periph_clock_enable(RCC_GPIOB);
	rcc_periph_clock_enable(RCC_GPIOA);
    rcc_periph_clock_enable(RCC_GPIOD);

    /* Enable clocks for USART1. */
    rcc_periph_clock_enable(RCC_USART1);

    rcc_periph_clock_enable(RCC_DMA2);
}

void encoder_setup(void)
{
    //gpio setup
    gpio_mode_setup(GPIOD, GPIO_MODE_AF, GPIO_PUPD_PULLUP, GPIO12);
    gpio_mode_setup(GPIOD, GPIO_MODE_AF, GPIO_PUPD_PULLUP, GPIO13);

    //gpio AF setup
    gpio_set_af(GPIOD, GPIO_AF2, GPIO12);
    gpio_set_af(GPIOD, GPIO_AF2, GPIO13);

    rcc_periph_clock_enable(RCC_TIM4);
    timer_set_period(TIM4, 2000);
    timer_slave_set_mode(TIM4, 0x3); // encoder
    timer_ic_set_input(TIM4, TIM_IC1, TIM_IC_IN_TI1);
    timer_ic_set_input(TIM4, TIM_IC2, TIM_IC_IN_TI2);
    timer_enable_counter(TIM4);
    //...
    //int motor_pos = timer_get_count(TIM3);
}

////
void usart1_isr(void)
{
    /* Check for IDLE line interrupt */

//
//    Bit 4 IDLE: IDLE line detected
//    This bit is set by hardware when an Idle Line is detected. An interrupt is generated if the
//    IDLEIE=1 in the USART_CR1 register. It is cleared by a software sequence (an read to the
//    USART_SR register followed by a read to the USART_DR register).
//    0: No Idle Line is detected
//    1: Idle Line is detected
//    Note: The IDLE bit will not be set again until the RXNE bit has been set itself (i.e. a new idle
//    line occurs).
//

    volatile uint32_t usart_sr = USART1_SR; // MUST be volatile
    volatile uint32_t usart_dr = USART1_DR;

    if( usart_sr & USART_SR_IDLE ) {              // we are called for IDLE line interrupt
//        BKPT;
        from = 1;
        usart_rx_check();
    }
}

//
static void usart_setup(void)
{
    /* Setup GPIO pins for USART1 transmit. */
    gpio_mode_setup(GPIOA, GPIO_MODE_AF, GPIO_PUPD_NONE, GPIO9);

    /* Setup GPIO pins for USART1 receive. */
    gpio_mode_setup(GPIOA, GPIO_MODE_AF, GPIO_PUPD_NONE, GPIO10);
    gpio_set_output_options(GPIOA, GPIO_OTYPE_OD, GPIO_OSPEED_50MHZ, GPIO10);

    /* Setup USART1 TX and RX pin as alternate function. */
    gpio_set_af(GPIOA, GPIO_AF7, GPIO9);
    gpio_set_af(GPIOA, GPIO_AF7, GPIO10);

    /* Setup USART1 parameters. */
    usart_set_baudrate(USART1, 2500000);
    usart_set_databits(USART1, 8);
    usart_set_stopbits(USART1, USART_STOPBITS_1);
    usart_set_mode(USART1, USART_MODE_TX_RX);
    usart_set_parity(USART1, USART_PARITY_NONE);
    usart_set_flow_control(USART1, USART_FLOWCONTROL_NONE);

    /* Enable USART1 Receive interrupt. */
    usart_enable_rx_interrupt(USART1);        //not needed in DMA mode ???

    /* Enable USART1 Transmit interrupt. */
    usart_enable_tx_interrupt(USART1);        //not needed in DMA mode ???

    /* Enable USART1 RX Idle interrupt. */
    usart_enable_rx_idle_interrupt(USART1);

//	/* Finally enable the USART. */
    usart_enable(USART1);

//    /* USART interrupt */
//    NVIC_SetPriority(USART3_IRQn, NVIC_EncodePriority(NVIC_GetPriorityGrouping(), 0, 0));
//    NVIC_EnableIRQ(USART3_IRQn);
    //USART1 RX
//    nvic_set_priority(NVIC_USART1_IRQ, 0);
//    nvic_enable_irq(NVIC_USART1_IRQ);
}

//
void dma_receive_setup(void)
{
    /*
     * Using channel 2 for USART1_RX
     */

    dma_stream_reset(DMA2, DMA_STREAM2);

    dma_set_peripheral_address(DMA2, DMA_STREAM2, (uint32_t) &USART1_DR);
    dma_set_transfer_mode(DMA2, DMA_STREAM2, DMA_SxCR_DIR_PERIPHERAL_TO_MEM);

    dma_set_peripheral_size(DMA2, DMA_STREAM2, DMA_SxCR_PSIZE_8BIT);
    dma_set_memory_size(DMA2, DMA_STREAM2, DMA_SxCR_MSIZE_8BIT);

    dma_set_priority(DMA2, DMA_STREAM2, DMA_SxCR_PL_VERY_HIGH);

    dma_disable_peripheral_increment_mode(DMA2, DMA_STREAM2);      //??
//    dma_disable_memory_increment_mode(DMA2, DMA_STREAM2);
    dma_enable_memory_increment_mode(DMA2, DMA_STREAM2);            //mozhe da ne triabva za 1 byte

    dma_enable_circular_mode(DMA2, DMA_STREAM2);
    dma_enable_direct_mode(DMA2, DMA_STREAM2);                  //FIFO disable

    dma_enable_transfer_complete_interrupt(DMA2, DMA_STREAM2);

    //enables/disables
    dma_enable_transfer_error_interrupt(DMA2, DMA_STREAM2);
    dma_enable_half_transfer_interrupt(DMA2, DMA_STREAM2);
    dma_enable_direct_mode_error_interrupt(DMA2, DMA_STREAM2);     //?
    dma_enable_fifo_error_interrupt(DMA2, DMA_STREAM2);

    //USART1 RX
    nvic_set_priority(NVIC_DMA2_STREAM2_IRQ, 0);
    nvic_enable_irq(NVIC_DMA2_STREAM2_IRQ);
}

//
void dma_receive(void* buffer, const int datasize)
{
    dma_set_memory_address(DMA2, DMA_STREAM2, (uint32_t) buffer);
    dma_set_number_of_data(DMA2, DMA_STREAM2, datasize);

    dma_channel_select(DMA2, DMA_STREAM2, DMA_SxCR_CHSEL_4);            //channel 4 is usart1_rx

    usart_enable_rx_dma(USART1);
    nvic_enable_irq(NVIC_DMA2_STREAM2_IRQ);                             //needed ?

    dma_enable_stream(DMA2, DMA_STREAM2);
}

//RX ISR
void dma2_stream2_isr(void)
{
//    uint32_t asdf = DMA2_LISR;                    //debug only

    //1 HALF TRANSFER COMPLETE
    if( DMA2_LISR & DMA_LISR_HTIF2 ) {              // STREAM 2
        DMA2_LIFCR |= DMA_LIFCR_CHTIF2;             //clear it in control register
        while(!(USART1_SR & USART_SR_TC));          //wait for transfer complete complete flag from periphery ... why ?
//        BKPT;
        from = 2;
        usart_rx_check();
        return;
    }
    //2 TRANSFER COMPLETE
    if( DMA2_LISR & DMA_LISR_TCIF2 ) {              //status register & tranfer complete flag
        DMA2_LIFCR |= DMA_LIFCR_CTCIF2;             //clear it in control register
        while(!(USART1_SR & USART_SR_TC));          //wait for transfer complete complete flag from periphery ... why ?

//        BKPT;
        from = 3;
        usart_rx_check();
        return;
    }
    //3 TRANSFER ERROR ?
    if( DMA2_LISR & DMA_LISR_TEIF2 ) {
        DMA2_LIFCR |= DMA_LIFCR_CTEIF2;             //clear it in control register
        BKPT;
        return;
    }
    //4 DIRECT MODE ERROR
    if( DMA2_LISR & DMA_LISR_DMEIF2 ) {
        DMA2_LIFCR |= DMA_LIFCR_CDMEIF2;            //clear it in control register
        BKPT;
        return;
    }
    //5 FIFO ERROR
    if( DMA2_LISR & DMA_LISR_FEIF2 ) {
        DMA2_LIFCR |= DMA_LIFCR_CFEIF2;             //clear it in control register
        BKPT;
        return;
    }

//    dma_clear_interrupt_flags(DMA2,DMA_STREAM2, 0xffffffff);

    BKPT;
    return;
}

//
static void dma_transmit_setup(void)
{
    /*
     * Using channel 7 for USART1_TX
     */

    /* Reset DMA channel*/
    dma_stream_reset(DMA2, DMA_STREAM7);

    dma_set_peripheral_address(DMA2, DMA_STREAM7, (uint32_t)&USART1_DR);

    dma_set_transfer_mode(DMA2, DMA_STREAM7, DMA_SxCR_DIR_MEM_TO_PERIPHERAL);

    dma_enable_memory_increment_mode(DMA2, DMA_STREAM7);
    dma_disable_peripheral_increment_mode(DMA2, DMA_STREAM7);      //??
    dma_set_peripheral_size(DMA2, DMA_STREAM7, DMA_SxCR_PSIZE_8BIT);     //was DMA_CCR_PSIZE_8BIT

    dma_set_memory_size(DMA2, DMA_STREAM7, DMA_SxCR_MSIZE_8BIT);         //was DMA_CCR_MSIZE_8BIT
    dma_set_priority(DMA2, DMA_STREAM7, DMA_SxCR_PL_VERY_HIGH);          //duplicate with uart_setup

//    dma_enable_circular_mode(DMA2, DMA_STREAM7);
    dma_enable_direct_mode(DMA2, DMA_STREAM7);                  //FIFO disable

    // disabled interrupts
    dma_enable_transfer_error_interrupt(DMA2, DMA_STREAM7);
    dma_enable_half_transfer_interrupt(DMA2, DMA_STREAM7);
    dma_enable_direct_mode_error_interrupt(DMA2, DMA_STREAM7);     //?
    dma_enable_fifo_error_interrupt(DMA2, DMA_STREAM7);
    dma_enable_transfer_complete_interrupt(DMA2, DMA_STREAM7);

    //USART1 TX from STM32CubeMX
    nvic_set_priority(NVIC_DMA2_STREAM7_IRQ, 0);
    nvic_enable_irq(NVIC_DMA2_STREAM7_IRQ);
}

static void dma_transmit(uint8_t *buffer, int datasize)
{
    gpio_set(GPIOB, GPIO12);        //DE set
    gpio_set(GPIOB, GPIO11);        //DE set debug

    dma_set_memory_address(DMA2, DMA_STREAM7, (uint32_t)buffer);
    dma_set_number_of_data(DMA2, DMA_STREAM7, datasize);

    dma_channel_select(DMA2, DMA_STREAM7, DMA_SxCR_CHSEL_4);

    usart_enable_tx_dma(USART1);
    nvic_enable_irq(NVIC_DMA2_STREAM7_IRQ);

    dma_enable_stream(DMA2, DMA_STREAM7);
}


//TX ISR
void dma2_stream7_isr(void)
{
    // HALF TRANSFER INTERRUPT
    if( DMA2_HISR & DMA_HISR_HTIF7 ) {              // STREAM 7
        DMA2_HIFCR |= DMA_HIFCR_CHTIF7;
        while(!(USART1_SR & USART_SR_TC));          //wait for transfer complete complete flag from periphery ... why ?
    }
    // FULL TRANSFER
    if( DMA2_HISR & DMA_HISR_TCIF7 ) {              //status register & tranfer complete flag
        DMA2_HIFCR |= DMA_HIFCR_CTCIF7;             //clear it in control register
        while(!(USART1_SR & USART_SR_TC));          //wait for transfer complete complete flag from periphery ... why ?
        gpio_clear(GPIOB, GPIO12);
        gpio_clear(GPIOB, GPIO11);        //DE reset
    }
    // TRANSFER ERROR
    if( DMA2_HISR & DMA_HISR_TEIF7 ) {
        DMA2_HIFCR |= DMA_HIFCR_CTEIF7;
        gpio_clear(GPIOB, GPIO12);
        gpio_clear(GPIOB, GPIO11);        //DE reset
        BKPT;           //debug only
    }
    // DIRECT MODE ERROR - FIFO
    if( DMA2_HISR & DMA_HISR_DMEIF7 ) {
        DMA2_HIFCR |= DMA_HIFCR_CDMEIF7;
        gpio_clear(GPIOB, GPIO12);
        gpio_clear(GPIOB, GPIO11);        //DE reset
        BKPT;           //debug only
    }
    // ???
    if( DMA2_HISR & DMA_HISR_FEIF7 ) {
        DMA2_HIFCR |= DMA_HIFCR_CFEIF7;
    }
}

//
static void gpio_setup(void)
{
    /* Setup GPIO pin GPIO12 on GPIO port B for DE. */
    gpio_mode_setup(GPIOB, GPIO_MODE_OUTPUT, GPIO_OTYPE_PP, GPIO12);
    gpio_clear(GPIOB, GPIO12);

    // just mirror for the scope
    gpio_mode_setup(GPIOB, GPIO_MODE_OUTPUT, GPIO_OTYPE_PP, GPIO11);
    gpio_clear(GPIOB, GPIO11);

    // just mirror for the scope2
    gpio_mode_setup(GPIOB, GPIO_MODE_OUTPUT, GPIO_OTYPE_PP, GPIO15);
    gpio_clear(GPIOB, GPIO15);

    //    LED1
    gpio_mode_setup(GPIOA, GPIO_MODE_OUTPUT, GPIO_OTYPE_PP, GPIO6);
    gpio_clear(GPIOA, GPIO6);

    //    LED2
    gpio_mode_setup(GPIOA, GPIO_MODE_OUTPUT, GPIO_OTYPE_PP, GPIO7);
    gpio_clear(GPIOA, GPIO7);

}

int dma_done(void)
{
    return !((DMA2_S2CR | DMA2_S7CR) & 1);
}


static void tim_setup(void)
{
    /* Enable TIM2 clock. */
    rcc_periph_clock_enable(RCC_TIM2);

    /* Enable TIM2 interrupt. */
    nvic_enable_irq(NVIC_TIM2_IRQ);

    /* Reset TIM2 peripheral to defaults. */
    rcc_periph_reset_pulse(RST_TIM2);

    /* Timer global mode:
     * - No divider
     * - Alignment edge
     * - Direction up
     * (These are actually default values after reset above, so this call
     * is strictly unnecessary, but demos the api for alternative settings)
     */
    timer_set_mode(TIM2, TIM_CR1_CKD_CK_INT,
        TIM_CR1_CMS_EDGE, TIM_CR1_DIR_UP);

    /*
     * Please take note that the clock source for STM32 timers
     * might not be the raw APB1/APB2 clocks.  In various conditions they
     * are doubled.  See the Reference Manual for full details!
     * In our case, TIM2 on APB1 is running at double frequency, so this
     * sets the prescaler to have the timer run at 5kHz
     */
    timer_set_prescaler(TIM2, ((rcc_apb1_frequency * 2) / 5000));

    /* Disable preload. */
    timer_disable_preload(TIM2);
    timer_continuous_mode(TIM2);

    /* count full range, as we'll update compare value continuously */
    timer_set_period(TIM2, 65535);

    /* Set the initual output compare value for OC1. */
    timer_set_oc_value(TIM2, TIM_OC1, 83570);

    /* Counter enable. */
    timer_enable_counter(TIM2);

    /* Enable Channel 1 compare interrupt to recalculate compare values */
    timer_enable_irq(TIM2, TIM_DIER_CC1IE);
}

void tim2_isr(void)
{
//    BKPT;
    if (timer_get_flag(TIM2, TIM_SR_CC1IF)) {

        /* Clear compare interrupt flag. */
        timer_clear_flag(TIM2, TIM_SR_CC1IF);

        /*
         * Get current timer value to calculate next
         * compare register value.
         */
        uint16_t compare_time = timer_get_counter(TIM2);

        /* Calculate and set the next compare value. */
        uint16_t frequency = 100;
        uint16_t new_time = compare_time + frequency;

        timer_set_oc_value(TIM2, TIM_OC1, new_time);

        if (++tim2_1 == 0) {
            if (++tim2_2 == 0) {
                if (++tim2_3 == 0) {
                    __asm__("NOP");
                }
            }
        }

        gpio_set(GPIOA, GPIO6);      //indicate RX packet
        gpio_set(GPIOA, GPIO7);      //indicate unknown packet
    }
}

//
int main(void)
{
	clock_setup();
	gpio_setup();
	usart_setup();
    encoder_setup();
    dma_receive_setup();
    dma_transmit_setup();

    tim_setup();          //do not mess for now

    motor_pos = motor_pos_old = TIM_CNT(TIM4);

    gpio_set(GPIOA, GPIO6);           //???? encoder
    gpio_set(GPIOA, GPIO7);           //???? encoder

//    BKPT;

    initialize_eeprom();

    for(uint32_t i=0; i<ARRAY_LEN(frame_buffer) ;i++) {
        frame_buffer[i] = 0x00;
    }

    for(uint32_t i=0; i<ARRAY_LEN(usart_rx_dma_buffer) ;i++) {
        usart_rx_dma_buffer[i] = 0x00;
    }

    dma_receive(&usart_rx_dma_buffer, ARRAY_LEN(usart_rx_dma_buffer));              // till 16 bytes frames is enough

    nvic_set_priority(NVIC_USART1_IRQ, 0);
    nvic_enable_irq(NVIC_USART1_IRQ);

    while (1) {
        for(int i=0;i<1000000; i++) {
            motor_pos = TIM_CNT(TIM4);
//            BKPT;
            if (motor_pos != motor_pos_old) {
//                BKPT;
                motor_pos_old = motor_pos;
                __asm__("NOP");
            }
            __asm__("NOP");
        }

        __asm__("NOP");
        __asm__("NOP");
        __asm__("NOP");
//        BKPT;
        //IMAME EA i BA komandi veche
        //EA - READ
        //BA - WRITE
	}

	return 0;
}

//
static uint8_t crcCalc(uint8_t* Data, uint8_t Size)
{
    uint8_t j;
    uint8_t Carry;
    uint8_t Crc;

    Crc = 0;

    while (Size-- > 0)
    {
        Crc ^= *Data++;
        for (j = 8; j != 0; j--)
        {
            Carry = Crc & 0x80;
            Crc <<= 1;
            if (Carry != 0)
            {
                Crc ^= 0x01;  /* Polynome X^8 + 1  */
            }
        }
    }
//    BKPT;
    return (Crc & 0x00FF);
}

/**
 * \brief           Check for new data received with DMA
 */
void usart_rx_check(void) {
    static uint8_t old_pos;
    uint8_t pos;

    /* Calculate current position in buffer */
    pos = ARRAY_LEN(usart_rx_dma_buffer) - dma_get_number_of_data(DMA2, DMA_STREAM2);
    if (pos != old_pos) {                       /* Check change in received data */
        if (pos > old_pos) {                    /* Current position is over previous one */
            /* We are in "linear" mode */
            /* Process data directly by subtracting "pointers" */
            usart_process_data(&usart_rx_dma_buffer[old_pos], pos - old_pos);
        } else {
            /* We are in "overflow" mode */
            /* First process data to the end of buffer */
            usart_process_data(&usart_rx_dma_buffer[old_pos], ARRAY_LEN(usart_rx_dma_buffer) - old_pos);
            /* Check and continue with beginning of buffer */
            if (pos > 0) {
                usart_process_data(&usart_rx_dma_buffer[0], pos);
            }
        }
    }
    old_pos = pos;                              /* Save current position as old */

    /* Check and manually update if we reached end of buffer */
    if (old_pos == ARRAY_LEN(usart_rx_dma_buffer)) {
        old_pos = 0;
    }
}

/**
 * \brief           Process received data over UART
 * \note            Either process them directly or copy to other bigger buffer
 * \param[in]       data: Data to process
 * \param[in]       len: Length in units of bytes
 */
void usart_process_data(const uint8_t* data, uint8_t len) {
    const uint8_t* d = data;

    gpio_set(GPIOB, GPIO15);                //debug     // trigger for the scope

    // copy buffer
    for(uint8_t i=0; i<len;i++) {
        frame_buffer[frame_buffer_pos++] = data[i];
    }
    gpio_clear(GPIOB, GPIO15);                //debug

    if (from == 1) {
        frame_buffer_pos = 0;
        __asm__("NOP");
        __asm__("NOP");
        __asm__("NOP");
    }

    if (from == 2) {
        __asm__("NOP");
        __asm__("NOP");
        __asm__("NOP");
    }

    if (from == 3) {
        __asm__("NOP");
        __asm__("NOP");
        __asm__("NOP");
    }

    // 0x52
    if ( data[0] == 0x52 )
    {
        gpio_clear(GPIOA, GPIO6);              //indicate RX packet

        gpio_set(GPIOB, GPIO15);                //debug
        gpio_clear(GPIOB, GPIO15);              //debug

        tmgw.tx.cf = 0x52;      //0x52 or 0x2a                  0x82 0x42
        tmgw.tx.sf = 0x83;      //status - no errors
        tmgw.tx.d0 = 0x9d;      //ot kyde go vzeh tova
        tmgw.tx.d1 = 0x03;
        tmgw.tx.d2 = 0x40;
        tmgw.tx.d3 = 0x94;
        tmgw.tx.d4 = 0x00;
        tmgw.tx.d5 = 0x80;

        tmgw.tx.len = 8;     //plus crc +1
        tmgw.tx.d6 = crcCalc(&tmgw.tx.cf, tmgw.tx.len);         //it;s checking the checksum :) correct

        dma_transmit(&tmgw.tx.cf, tmgw.tx.len+1);       //+crc
        return;
    }

    // 0xEA EEPROM READ
    if ( frame_buffer[0] == 0xea ) {
        //make additional checks
        gpio_clear(GPIOA, GPIO6);              //indicate RX packet

        tmgw.tx2.cf = 0xea;
        tmgw.tx2.sf = frame_buffer[1];
        tmgw.tx2.d0 = eeprom[frame_buffer[1]];

        tmgw.tx2.len = 3;                       //plus crc +1
        tmgw.tx2.d1 = crcCalc(&tmgw.tx2.cf, tmgw.tx2.len);

        dma_transmit(&tmgw.tx2.cf, tmgw.tx2.len+1);
        return;
    }

    // 0x2A
    if ( frame_buffer[0] == 0x2a )
    {
        gpio_clear(GPIOA, GPIO6);              //indicate RX packet

        tmgw.tx2.cf = 0x2a;
        tmgw.tx2.sf = 0x83;

//      here
//        tmgw.tx2.d0 = tim2_1;             //updates from tim2
//        tmgw.tx2.d1 = tim2_2;             //updates from tim2
//        tmgw.tx2.d2 = tim2_3;             //updates from tim2

        tmgw.tx2.d0 = TIM4_CNT & 0x00ff;
        tmgw.tx2.d1 = (TIM4_CNT & 0xff00) >> 8;
        tmgw.tx2.d2 = (TIM4_CNT & 0xff0000) >> 16;

//        tmgw.tx2.d3 = 0x00;
//        tmgw.tx2.d4 = 0x00;

        tmgw.tx2.d3 = TIM4_CNT & 0x00ff;
        tmgw.tx2.d4 = (TIM4_CNT & 0xff00) >> 8;

        tmgw.tx2.d5 = 0x00;

        tmgw.tx2.len = 8;                       //plus crc +1
        tmgw.tx2.d6 = crcCalc(&tmgw.tx2.cf, tmgw.tx2.len);

        dma_transmit(&tmgw.tx2.cf, tmgw.tx2.len+1);
        return;
    }

    // 0xC2
    if ( frame_buffer[0] == 0xc2 )
    {
        gpio_clear(GPIOA, GPIO6);              //indicate RX packet

        tmgw.tx2.cf = 0xc2;
        tmgw.tx2.sf = 0x83;
//        tmgw.tx2.d0 = 0x02;             //updates from tim2
//        tmgw.tx2.d1 = 0x00;             //updates from tim2
//        tmgw.tx2.d2 = 0x02;             //updates from tim2
        tmgw.tx2.d3 = 0x00;
        tmgw.tx2.d4 = 0x00;
        tmgw.tx2.d5 = 0x00;

        tmgw.tx2.len = 8;                       //plus crc +1
        tmgw.tx2.d6 = crcCalc(&tmgw.tx2.cf, tmgw.tx2.len);

        dma_transmit(&tmgw.tx2.cf, tmgw.tx2.len+1);
        return;
    }
    // for indicating unknown packet
    gpio_clear(GPIOA, GPIO7);
}

void initialize_eeprom()
{
    //MHMD042P1U parsen be logicdata from .... nice guy
    eeprom[0x00] = 0x00;
    eeprom[0x01] = 0x01;
    eeprom[0x02] = 0x08;
    eeprom[0x03] = 0x09;
    eeprom[0x04] = 0xBA;
    eeprom[0x05] = 0x03;
    eeprom[0x06] = 0x70;
    eeprom[0x07] = 0x17;
    eeprom[0x08] = 0x00;
    eeprom[0x09] = 0xFF;
    eeprom[0x0A] = 0xFE;
    eeprom[0x0B] = 0xF9;
    eeprom[0x0C] = 0xFC;
    eeprom[0x0D] = 0xFE;
    eeprom[0x0E] = 0xFB;
    eeprom[0x0F] = 0xBF;
    eeprom[0x10] = 0x4D;
    eeprom[0x11] = 0x48;
    eeprom[0x12] = 0x4D;
    eeprom[0x13] = 0x44;
    eeprom[0x14] = 0x30;
    eeprom[0x15] = 0x34;
    eeprom[0x16] = 0x32;
    eeprom[0x17] = 0x50;
    eeprom[0x18] = 0x31;
    eeprom[0x19] = 0x53;
    eeprom[0x1A] = 0x00;
    eeprom[0x1B] = 0x00;
    eeprom[0x1C] = 0x35;
    eeprom[0x1D] = 0x00;
    eeprom[0x1E] = 0x00;
    eeprom[0x1F] = 0x3B;
    eeprom[0x20] = 0x1A;
    eeprom[0x21] = 0x00;
    eeprom[0x22] = 0x7F;
    eeprom[0x23] = 0x00;
    eeprom[0x24] = 0x2C;
    eeprom[0x25] = 0x01;
    eeprom[0x26] = 0x9E;
    eeprom[0x27] = 0x02;
    eeprom[0x28] = 0x7C;
    eeprom[0x29] = 0x01;
    eeprom[0x2A] = 0xB2;
    eeprom[0x2B] = 0x00;
    eeprom[0x2C] = 0x17;
    eeprom[0x2D] = 0x02;
    eeprom[0x2E] = 0x04;
    eeprom[0x2F] = 0x4E;
    eeprom[0x30] = 0x00;
    eeprom[0x31] = 0x00;
    eeprom[0x32] = 0x00;
    eeprom[0x33] = 0x00;
    eeprom[0x34] = 0x00;
    eeprom[0x35] = 0x00;
    eeprom[0x36] = 0x00;
    eeprom[0x37] = 0x00;
    eeprom[0x38] = 0x00;
    eeprom[0x39] = 0x00;
    eeprom[0x3A] = 0x00;
    eeprom[0x3B] = 0x00;
    eeprom[0x3C] = 0x00;
    eeprom[0x3D] = 0x00;
    eeprom[0x3E] = 0x00;
    eeprom[0x3F] = 0x00;
    eeprom[0x40] = 0x00;
    eeprom[0x41] = 0x02;
    eeprom[0x42] = 0xC3;
    eeprom[0x43] = 0x00;
    eeprom[0x44] = 0x82;
    eeprom[0x45] = 0x00;
    eeprom[0x46] = 0x00;
    eeprom[0x47] = 0x00;
    eeprom[0x48] = 0xA0;
    eeprom[0x49] = 0x00;
    eeprom[0x4A] = 0x03;
    eeprom[0x4B] = 0x00;
    eeprom[0x4C] = 0x64;
    eeprom[0x4D] = 0x17;
    eeprom[0x4E] = 0xC8;
    eeprom[0x4F] = 0xD3;
    eeprom[0x50] = 0x71;
    eeprom[0x51] = 0x0F;
    eeprom[0x52] = 0x27;
    eeprom[0x53] = 0x00;
    eeprom[0x54] = 0x08;
    eeprom[0x55] = 0x01;
    eeprom[0x56] = 0x01;
    eeprom[0x57] = 0xFF;
    eeprom[0x58] = 0x01;
    eeprom[0x59] = 0x08;
    eeprom[0x5A] = 0x09;
    eeprom[0x5B] = 0xB6;
    eeprom[0x5C] = 0x2E;
    eeprom[0x5D] = 0x00;
    eeprom[0x5E] = 0x00;
    eeprom[0x5F] = 0x5A;
    eeprom[0x60] = 0x00;
    eeprom[0x61] = 0x00;
    eeprom[0x62] = 0x00;
    eeprom[0x63] = 0x00;
    eeprom[0x64] = 0x00;
    eeprom[0x65] = 0x00;
    eeprom[0x66] = 0x00;
    eeprom[0x67] = 0x00;
    eeprom[0x68] = 0x00;
    eeprom[0x69] = 0x00;
    eeprom[0x6A] = 0x00;
    eeprom[0x6B] = 0x00;
    eeprom[0x6C] = 0x00;
    eeprom[0x6D] = 0x00;
    eeprom[0x6E] = 0x00;
    eeprom[0x6F] = 0x00;
    eeprom[0x70] = 0x00;
    eeprom[0x71] = 0x00;
    eeprom[0x72] = 0x00;
    eeprom[0x73] = 0x00;
    eeprom[0x74] = 0x00;
    eeprom[0x75] = 0x00;
    eeprom[0x76] = 0x00;
    eeprom[0x77] = 0x00;
    eeprom[0x78] = 0x00;
    eeprom[0x79] = 0x00;
    eeprom[0x7A] = 0x00;
    eeprom[0x7B] = 0x00;
    eeprom[0x7C] = 0x00;
    eeprom[0x7D] = 0x00;
    eeprom[0x7E] = 0x00;
    //starting with 4f tries to write in 7f 55 aa sequentaly for eeprom damage ???
}


// 3 x 0x52 responses
// 0xEA READ EEPROM from 00 do 7E
// after reading 0x4F the follwing begins
// BA 7F 55 90
// BA 7F AA 6F
// seems like they want to damage the eeprom ? ;)
// pause 176ms
// 25 0x52 with responses
// 0x2a ....
// 0xc2 appeared and drive gave 44 error. clear from panaterm
// ToDo: crccalc on frames arrived
