set target-async on
set mem inaccessible-by-default off
target extended-remote /dev/ttyBmpGdb
#target extended-remote tcp:localhost:2000
#monitor traceswo
monitor swdp_scan
#monitor traceswo
#set debug remote 1
attach 1
stop
#monitor traceswo 2250000

#file ./build/modbus.elf 
load ./usart_irq.hex 
compare-sections
#hbreak main
#hbreak Core/Src/main.c:258
#next
#watch huart1
#watch data[0]
#python sys.path.insert(1, '/home/smooker/src/qt-creator/share/qtcreator/debugger/')
#python sys.path.append('/home/smooker/src/stm32/gcc-arm-none-eabi-9-2020-q2-update/bin/data-directory/python')
#python from gdbbridge import *
#python theDumper.loadDumpers({"token":11})
#12-interpreter-exec console \"set target-async off\"
#show debug-file-directory
#set debug arm on
#show debug arm
watch tmgw.rx.cf
#watch tmgw.rx.sf
#watch tmgw.rx.d0
#watch tmgw.rx.d1
run
#monitor hard_srst


