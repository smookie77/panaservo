# README

This example program echoes data sent in on USART2 on the
ST STM32F4DISCOVERY eval board. Uses interrupts for that purpose.

The sending is done in a nonblocking way.

## Board connections

| Port  | Function      | Description                       |
| ----- | ------------- | --------------------------------- |
| `PA2` | `(USART2_TX)` | TTL serial output `(115200,8,N,1)` |
| `PA3` | `(USART2_RX)` | TTL serial input `(115200,8,N,1)`  |


/* Memory map for all busses */
#define PERIPH_BASE			(0x40000000U)
#define PERIPH_BASE_APB1		(PERIPH_BASE + 0x00000)
#define PERIPH_BASE_APB2		(PERIPH_BASE + 0x10000)
#define PERIPH_BASE_AHB1		(PERIPH_BASE + 0x20000)
#define PERIPH_BASE_AHB2		0x50000000U
#define PERIPH_BASE_AHB3		0x60000000U


#define PERIPH_BASE_APB2		(PERIPH_BASE + 0x10000)

#define USART1_BASE			(PERIPH_BASE_APB2 + 0x1000)

0x40000000U
   0x10000
    0x1000
    
0x40011000    
